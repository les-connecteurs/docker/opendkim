# OpenDKIM

A custom Docker image with OpenDKIM.

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
