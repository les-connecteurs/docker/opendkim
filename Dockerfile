FROM registry.gitlab.com/les-connecteurs/docker/alpine

EXPOSE 8891

# configuration
ENV SIGNING_TABLE=refile:/etc/opendkim/SigningTable
ENV KEY_TABLE=csl:keyname=example.com:selector:/etc/opendkim/opendkim.key

# install dependencies
RUN apk add --no-cache \
  gettext \
  opendkim \
  openssl

RUN install -m 0755 -o opendkim -g opendkim -d /run/opendkim/
RUN rm -f /etc/opendkim/opendkim.conf

# generate default keys
RUN openssl genrsa -out /etc/opendkim/opendkim.key 1024
RUN openssl rsa -in /etc/opendkim/opendkim.key -pubout -out /etc/opendkim/opendkim.pub.key

# copy configuration file
COPY opendkim.conf /etc/opendkim/opendkim-template.conf
COPY SigningTable /etc/opendkim/SigningTable

# init script
COPY init.sh /init
RUN chmod +x /init
CMD ["/init"]
