#!/bin/sh

# generate configuration files
if [ ! -f "/etc/opendkim/opendkim.conf" ]; then
  envsubst \
    < /etc/opendkim/opendkim-template.conf \
    > /etc/opendkim/opendkim.conf
fi

syslogd -O - -n &
opendkim -f
